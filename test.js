import http from 'k6/http';
import { check } from 'k6';
import SharedArray from 'k6/data';
import { Rate, Counter } from 'k6/metrics';

const BASE_URL = 'https://answeron.fx2.io/api/v1/questions/';

const REQUEST_OPTIONS = {
  headers: {
    'Accept': 'application/json, text/plain, */*',
    'Content-Type': 'application/json'
  }
};

/** metric */
const study_rate = new Rate('_study_rate');
const wyzant_rate = new Rate('_wyzant_rate');
const brainly_rate = new Rate('_brainly_rate');
const gauthmath_rate = new Rate('_gauthmath_rate');

const posted_questions = new Counter('_posted_questions');

/** @see https://k6.io/docs/using-k6/k6-options/reference */
export const options = {
  scenarios: {
    answeron: {
      executor: 'constant-vus',
      vus: 1,
      duration: '2m',
      gracefulStop: '5m'
    },
  },
  thresholds: {
    checks: ['rate>0.99'],
  },
}

export const fixtures = JSON.parse(open('./fixtures/questions.json'));

/** test */
export default function() {
  let payload = JSON.stringify(fixtures[Math.floor(Math.random() * fixtures.length)]);
  let response = http.post(BASE_URL, payload, REQUEST_OPTIONS);

  console.log('payload:', payload);

  check(response, {'Question registered.': (r) => r.status === 200 && r.json('id').length !== 0});
  
  console.log('answeron-question-id:', response.json('id'));

  let answers = _wait_answer(response.json('id'));

  check(answers, {'Answer found.': (a) => a.length !== 0});

  console.log('answers:', answers);


  _record_service_rate(answers);
}

export function _wait_answer(questionId) {
  let response = http.get(BASE_URL + questionId, REQUEST_OPTIONS);

  if (response.json('answers').length === 0) {
    return _wait_answer(questionId);
  }

  let answers = Object.values(response.json('answers')).filter((a) => a.text !== 'no result');

  if (answers.length === 0) {
    return _wait_answer(questionId);
  }

  return answers;
}

export function _record_service_rate(answers) {
  Object.values(answers).forEach((a) => {
    switch(a.source) {
      case 'wyzant':
        wyzant_rate.add(true);
        study_rate.add(false);
        brainly_rate.add(false);
        gauthmath_rate.add(false);

        console.log('found answer:', 'wyzant');

        break;
      case 'study':
        wyzant_rate.add(false);
        study_rate.add(true);
        brainly_rate.add(false);
        gauthmath_rate.add(false);

        console.log('found answer:', 'study');

        break;
      case 'brainly':
        wyzant_rate.add(false);
        study_rate.add(false);
        brainly_rate.add(true);
        gauthmath_rate.add(false);

        console.log('found answer:', 'brainly');

        break;
      case 'gauthmath':
        wyzant_rate.add(false);
        study_rate.add(false);
        brainly_rate.add(false);
        gauthmath_rate.add(true);

        console.log('found answer:', 'gauthmath');

        break;
      default:
        return;
    };
  });
}
