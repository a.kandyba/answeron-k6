# ANSWERON-K6
https://k6.io/docs/

## :pizza: Требования
* [make (v3.81 or late)](https://savannah.gnu.org/projects/make/)
* [docker (v19.03.12 or late)](https://docs.docker.com/engine/install/ubuntu/)
* [docker-compose (v1.26.0 or late)](https://docs.docker.com/compose/install/)

## :dancer: Использование

### 1. Клонируем репозиторий
```bash
> git clone git@gitlab.com:a.kandyba/answeron-k6.git
```

### 2. Запускаем тест
```bash
$PROJECT/make test
```

### Make
Чтобы увидеть список доступных команд, выполните `$PROJECT/make`.
