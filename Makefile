# Make stuff
.DEFAULT_GOAL := help
.PHONY: help

# Available commands:
help:
	@grep -E '^[a-zA-Z-]+:.*?## .*$$' Makefile | awk 'BEGIN {FS = ":.*?## "}; {printf "[32m%-27s[0m %s\n", $$1, $$2}'

test: ## Run test container.
	@docker-compose up
